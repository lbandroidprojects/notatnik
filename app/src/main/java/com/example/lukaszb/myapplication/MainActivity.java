package com.example.lukaszb.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static final int GETNOTEREQUEST=3;
    Button bNotatka;
    ListView lv;
    ArrayList<String> lNotatki;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState!=null)
          lNotatki=savedInstanceState.getStringArrayList("notatki");
        else
          lNotatki=new ArrayList<>();

        setContentView(R.layout.activity_main);

        bNotatka=(Button)findViewById(R.id.bNowa);
        bNotatka.setOnClickListener(this);
        lv=(ListView)findViewById(R.id.listView);


        adapter=
                new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,lNotatki);
        lv.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        Intent in=new Intent(this,NoteActivity.class);
        startActivityForResult(in,GETNOTEREQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==GETNOTEREQUEST && resultCode== Activity.RESULT_OK){
            String text=data.getStringExtra("notatka");
            if (text!=null)
            {
                lNotatki.add(text);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("notatki",lNotatki);
    }
}
