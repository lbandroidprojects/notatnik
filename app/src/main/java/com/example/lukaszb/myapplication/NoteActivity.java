package com.example.lukaszb.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NoteActivity extends AppCompatActivity implements View.OnClickListener{
    Button bOK,bAnuluj;
    EditText etNotatka;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        bOK=(Button)findViewById(R.id.bOK);
        bAnuluj=(Button)findViewById(R.id.bAnuluj);

        bOK.setOnClickListener(this);
        bAnuluj.setOnClickListener(this);

        etNotatka=(EditText)findViewById(R.id.etNotatka);
    }

    @Override
    public void onClick(View view) {
       switch (view.getId()){
           case R.id.bOK:{
                if(etNotatka.getText().length()>0){
                    Intent in=new Intent();
                    in.putExtra("notatka",etNotatka.getText().toString());
                    setResult(Activity.RESULT_OK,in);
                    finish();
                }
                else
                    Toast.makeText(this,"Nie można zapisać pustej notatki",Toast.LENGTH_SHORT).show();
               break;
           }
           case R.id.bAnuluj:{
               setResult(Activity.RESULT_CANCELED);
               finish();
           }
       }
    }
}
